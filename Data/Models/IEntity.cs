﻿using System;

namespace HandyComponents.Data.Models
{
    public interface IIDEntity
    {
        int ID { get; set; }
    }

    public interface IGuidEntity
    {
        Guid ID { get; set; }
    }

    public interface IBaseEntityDataCore
    {
        DateTimeOffset CreatedAt { get; set; }
        DateTimeOffset UpdatedAt { get; set; }
        int? UpdatedByID { get; set; }
        int RecordVersion { get; set; }
    }

    public interface IBaseEntity : IIDEntity, IBaseEntityDataCore
    {
    }

    public interface IBaseGuidEntity : IGuidEntity
    {
        DateTimeOffset CreatedAt { get; set; }
        DateTimeOffset UpdatedAt { get; set; }
        int? UpdatedByID { get; set; }
        int RecordVersion { get; set; }
    }

    //History tracking
    public interface IEntityHistoryDataCore
    {
        int? DeletedByID { get; set; }
        DateTimeOffset? DeletedAt { get; set; }
    }

    public interface IEntityHistory : IIDEntity, IEntityHistoryDataCore
    {
        int BaseTableID { get; set; }
    }

    public interface IGuidEntityHistory : IGuidEntity, IEntityHistoryDataCore
    {
        Guid BaseTableID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HandyComponents.Data.Extensions
{
    public static class ContextExtensions
    {
        public static IEnumerable<string> GetTableName(this DbContext context, Type type)
        {
            var metadata = ((IObjectContextAdapter)context).ObjectContext.MetadataWorkspace;
            // Get the part of the model that contains info about the actual CLR types
            var objectItemCollection = ((ObjectItemCollection)metadata.GetItemCollection(DataSpace.OSpace));

            // Get the entity type from the model that maps to the CLR type 
            var entityType = metadata
                .GetItems<EntityType>(DataSpace.OSpace)
                .Single(e => objectItemCollection.GetClrType(e) == type);

            // Get the entity set that uses this entity type 
            var entitySet = metadata
            .GetItems<EntityContainer>(DataSpace.CSpace)
                .Single()
                .EntitySets
                .Single(s => s.ElementType.Name == entityType.Name);

            // Find the mapping between conceptual and storage model for this entity set 
            var mapping = metadata.GetItems<EntityContainerMapping>(DataSpace.CSSpace)
                .Single()
                .EntitySetMappings
                .Single(s => s.EntitySet == entitySet);

            // Find the storage entity sets (tables) that the entity is mapped var tables = mapping
            var tables = mapping
                .EntityTypeMappings.Single()
                .Fragments;

            // Return the table name from the storage entity set
            return tables.Select(f => (string)f.StoreEntitySet.MetadataProperties["Table"].Value ?? f.StoreEntitySet.Name);
        }

        public static string GetTableName<T>(this DbContext context, bool includeSchema = true) where T : class
        {
            ObjectContext objectContext = ((IObjectContextAdapter)context).ObjectContext; return objectContext.GetTableName(typeof(T), includeSchema);
        }
        public static string GetTableName(this DbContext context, Type t, bool includeSchema = true)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)context).ObjectContext;
            return objectContext.GetTableName(t, includeSchema);
        }

        private static readonly Dictionary<Type, string> TableNames = new Dictionary<Type, string>();

        public static string GetTableName(this ObjectContext context, Type t, bool includeSchema = true)
        {
            string result;

            if (!TableNames.TryGetValue(t, out result))
            {
                lock (TableNames)
                {
                    if (!TableNames.TryGetValue(t, out result))
                    {
                        string entityName = t.Name;
                        ReadOnlyCollection<EntityContainerMapping> storageMetadata =
                            context.MetadataWorkspace.GetItems<EntityContainerMapping>(DataSpace.CSSpace);

                        foreach (EntityContainerMapping ecm in storageMetadata)
                        {
                            EntitySet entitySet;
                            if (ecm.StoreEntityContainer.TryGetEntitySetByName(entityName, true, out entitySet))
                            {
                                if (includeSchema)
                                    result = entitySet.Schema + "." + entitySet.Table; //TODO: brackets
                                else
                                    result = entitySet.Table; //TODO: brackets break;
                            }
                        }
                        TableNames.Add(t, result);
                    }
                }
            }
            return result;
        }

        public static int SaveChangesWithErrors(this DbContext context)
        {
            try
            {
                return context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation \n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }
                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" + sb, ex
                    ); // Add the original exception as the innerException
            }
        }

        public async static Task<int> SaveChangesWithErrorsAsync(this DbContext context)
        {
            try
            {
                return await context.SaveChangesAsync();
            }
            catch (DbEntityValidationException ex)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation \n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }
                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" + sb, ex
                    ); // Add the original exception as the innerException
            }
        }

        public static int ExecuteSql(this DbContext context, string sql)
        {
            return context.Database.ExecuteSqlCommand(sql);
        }

        public static IEnumerable<int> TruncateTable<TElement>(this DbContext context, string sql)
        where TElement : class
        {
            IEnumerable<string> linkedTables = context.GetTableName(typeof(TElement));
            List<int> result = new List<int>();
            foreach (string linkedTable in linkedTables)
            {
                result.Add(context.Database.ExecuteSqlCommand("TRUNCATE TABLE " + linkedTable));
            }
            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace HandyComponents.Data.Extensions
{
    public static class PropertiesHelper
    {
        public static string GetDisplayName<TModel>(Expression<Func<TModel, object>> expression)
        {
            Type type = typeof(TModel);
            string propertyName = null;
            string[] properties = null;
            IEnumerable<string> propertyList;
            //unless it’s a root property the expression NodeType will always be Convert 
            switch (expression.Body.NodeType)
            {
                case ExpressionType.Convert:
                case ExpressionType.ConvertChecked:
                    var ue = expression.Body as UnaryExpression;
                    propertyList = (ue != null ? ue.Operand : null).ToString().Split(".".ToCharArray()).Skip(1);
                    //don't use the root property 
                    break;
                default:
                    propertyList = expression.Body.ToString().Split(".".ToCharArray()).Skip(1);
                    break;
            }
            //the propert name is what we’re after 
            propertyName = propertyList.Last();
            //list of properties - the last property name
            properties = propertyList.Take(propertyList.Count() - 1).ToArray(); //grab all the parent properties
            Expression expr = null;
            foreach (string property in properties)
            {
                PropertyInfo propertylnfo = type.GetProperty(property);
                expr = Expression.Property(expr, type.GetProperty(property));
                type = propertylnfo.PropertyType;
            }
            return GetDisplayName(type.GetProperty(propertyName));
        }

        public static string GetDisplayName(PropertyInfo field)
        {
            string returnValue = String.Empty;
            DisplayAttribute attr =
                (DisplayAttribute)field.GetCustomAttributes(typeof(DisplayAttribute), true).SingleOrDefault();

            // Look for [MetadataType] attribute in type hierarchy
            // http://stackoverflow.com/questions/1910532/attribute-isdefined-doesnt-see-attributes-applied-with-metadatatvpe-class 
            if (attr == null)
            {
                MetadataTypeAttribute metadataType =
                    (MetadataTypeAttribute)
                        field.GetCustomAttributes(typeof(MetadataTypeAttribute), true).FirstOrDefault();
                if (metadataType != null)
                {
                    var property = metadataType.MetadataClassType.GetProperty(field.Name);
                    if (property != null)
                    {
                        attr =
                            (DisplayAttribute)
                                property.GetCustomAttributes(typeof(DisplayNameAttribute), true).SingleOrDefault();
                    }
                }
            }
            if (attr != null && attr.ResourceType != null)
                returnValue = attr.ResourceType.GetProperty(attr.Name).GetValue(attr).ToString();
            else if (attr != null)
                returnValue = attr.Name;
            if (String.IsNullOrEmpty(returnValue)) return field.Name;
            return returnValue;
        }

        public static string GetFullPropertyName<T>(Expression<Func<T, object>> exp)
        {
            MemberExpression memberExp;
            if (!TryFindMemberExpression(exp.Body, out memberExp)) return string.Empty;
            var memberNames = new Stack<string>();
            do
            {
                memberNames.Push(memberExp.Member.Name);
            } while (TryFindMemberExpression(memberExp.Expression, out memberExp));
            return string.Join(".", memberNames.ToArray());
        }

        private static bool TryFindMemberExpression(Expression exp, out MemberExpression memberExp)
        {
            memberExp = exp as MemberExpression;
            if (memberExp != null)
            {
                // heyo! that was easy enough 
                return true;
            }
            // if the compiler created an automatic conversion,
            // it’ll look something like...
            // obj => Convert(obj.Property) [e.g., int -> object]
            // OR:
            // obj => ConvertChecked(obj.Property) [e.g., int -> long]
            // ...which are the cases checked in IsConversion 
            if (IsConversion(exp) && exp is UnaryExpression)
            {
                memberExp = ((UnaryExpression)exp).Operand as MemberExpression;
                if (memberExp != null)
                {
                    return true;
                }
            }
            return false;
        }

        private static bool IsConversion(Expression exp)
        {
            return (exp.NodeType == ExpressionType.Convert | exp.NodeType == ExpressionType.ConvertChecked);
        }
    }
}

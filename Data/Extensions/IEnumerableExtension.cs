﻿using System;
using System.Collections.Generic;

namespace HandyComponents.Data.Extensions
{
    public static class IEnumerableExtension
    {
        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action) where T : class
        {
            foreach (T item in enumerable)
            {
                action(item);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using HandyComponents.Data.Extensions;
using HandyComponents.Data.Models;

namespace HandyComponents.Data.DataServices
{
    /// <summary>
    /// Wrapper for Entity Framework Context with additional functions
    /// </summary>
    public class GenericRepository : IGenericRepository
    {
        internal static NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public DbContext Connection { get; private set; }

        public GenericRepository(DbContext connection)
        {
            Connection = connection;
            //Connection = new NotificationServiceContext();
        }

        /// <summary>
        /// Detache model from database context (changes in the model will not be saved after that)
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="model"></param>
        public void DetacheModel<TEntity>(ref TEntity model) where TEntity : class
        {
            Connection.Entry(model).State = EntityState.Detached;
        }

        /// <summary>
        /// Get Queryable entity set
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="condition"></param>
        /// <returns></returns>
        public IQueryable<TEntity> GetQueryable<TEntity>(Expression<Func<TEntity, bool>> condition = null)
            where TEntity : class
        {
            IQueryable<TEntity> result = Connection.Set<TEntity>();
            if (condition != null)
                result = result.Where(condition);
            return result;
        }

        /// <summary>
        /// Get entity by key async
        /// </summary>
        /// <typeparam name="TEntity">Any entity type in the database</typeparam>
        /// <param name="key">ID int</param>
        /// <returns>Entity</returns>
        public async Task<TEntity> FindAsync<TEntity>(int key)
            where TEntity : class
        {
            TEntity result = await Connection.Set<TEntity>().FindAsync(key);
            return result;
        }

        /// <summary>
        /// Update entity with history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="model">Entity of base type where key ID property has an int type</param>
        /// <param name="internalUserID">User ID in the application database</param>
        public void Create<TEntity, TEntityHistory>(TEntity model, int? internalUserID)
            where TEntity : class, IBaseEntity
            where TEntityHistory : class, IBaseEntity, IEntityHistory, new()
        {
            RecordModelChanges<TEntity, TEntityHistory>(model, internalUserID);
            Connection.Set<TEntity>().Add(model);
        }

        /// <summary>
        /// Update entity where key ID property has a Guid type with history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="model">Entity of base type where key ID property has a Guid type</param>
        /// <param name="internalUserID">User ID in the application database</param>
        public void CreateGuid<TEntity, TEntityHistory>(TEntity model, int? internalUserID)
            where TEntity : class, IBaseGuidEntity
            where TEntityHistory : class, IBaseGuidEntity, IGuidEntityHistory, new()
        {
            RecordModelChangesGuid<TEntity, TEntityHistory>(model, internalUserID);
            Connection.Set<TEntity>().Add(model);
        }

        /// <summary>
        /// Update entity without history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="model">Entity of base type where key ID property has an int type</param>
        public void Create<TEntity>(TEntity model) where TEntity : class
        {
            Connection.Set<TEntity>().Add(model);
        }



        /// <summary>
        /// Update entity with history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="model">Entity of base type where key ID property has an int type</param>
        /// <param name="internalUserID">User ID in the application database</param>
        public void Update<TEntity, TEntityHistory>(TEntity model, int? internalUserID)
            where TEntity : class, IBaseEntity
            where TEntityHistory : class, IBaseEntity, IEntityHistory, new()
        {
            RecordModelChanges<TEntity, TEntityHistory>(model, internalUserID);
        }

        /// <summary>
        /// Update entities with history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="models">Entities of base type where key ID property has an int type</param>
        /// <param name="internalUserID">User ID in the application database</param>
        public void Update<TEntity, TEntityHistory>(IEnumerable<TEntity> models, int? internalUserID)
            where TEntity : class, IBaseEntity
            where TEntityHistory : class, IBaseEntity, IEntityHistory, new()
        {
            RecordModelChanges<TEntity, TEntityHistory>(models, internalUserID);
        }

        /// <summary>
        /// Update entity where key ID property has a Guid type with history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="model">Entity of base type where key ID property has a Guid type</param>
        /// <param name="internalUserID">User ID in the application database</param>
        public void UpdateGuid<TEntity, TEntityHistory>(TEntity model, int? internalUserID)
            where TEntity : class, IBaseGuidEntity
            where TEntityHistory : class, IBaseGuidEntity, IGuidEntityHistory, new()
        {
            RecordModelChangesGuid<TEntity, TEntityHistory>(model, internalUserID);
        }

        /// <summary>
        /// Update entities where key ID property has a Guid type with history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="models">Entities of base type where key ID property has a Guid type</param>
        /// <param name="internalUserID">User ID in the application database</param>
        public void UpdateGuid<TEntity, TEntityHistory>(IEnumerable<TEntity> models, int? internalUserID)
            where TEntity : class, IBaseGuidEntity
            where TEntityHistory : class, IBaseGuidEntity, IGuidEntityHistory, new()
        {
            RecordModelChangesGuid<TEntity, TEntityHistory>(models, internalUserID);
        }

        /// <summary>
        /// Delete entity with history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="model">Entity of base type where key ID property has an int type</param>
        /// <param name="internalUserID">User ID in the application database</param>
        public void Delete<TEntity, TEntityHistory>(TEntity model, int? internalUserID)
            where TEntity : class, IBaseEntity
            where TEntityHistory : class, IBaseEntity, IEntityHistory, new()
        {
            RecordModelChanges<TEntity, TEntityHistory>(model, internalUserID, true);
            Connection.Set<TEntity>().Remove(model);
        }

        /// <summary>
        /// Delete entity where key ID property has a Guid type with history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="model">Entity of base type where key ID property has a Guid type</param>
        /// <param name="internalUserID">User ID in the application database</param>
        public void DeleteGuid<TEntity, TEntityHistory>(TEntity model, int? internalUserID)
            where TEntity : class, IBaseGuidEntity
            where TEntityHistory : class, IBaseGuidEntity, IGuidEntityHistory, new()
        {
            RecordModelChangesGuid<TEntity, TEntityHistory>(model, internalUserID, true);
            Connection.Set<TEntity>().Remove(model);
        }

        /// <summary>
        /// Delete entity without history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="model">Entity of base type where key ID property has an int type</param>
        public void Delete<TEntity>(TEntity model) where TEntity : class
        {
            Connection.Set<TEntity>().Remove(model);
        }

        /// <summary>
        /// Delete All Elements
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="condition">Delete only entities satisfying the condition</param>|
        /// <param name="internalUserID">User ID in the application database</param>
        /// <returns>Number of deleted entities</returns>
        public int DeleteAll<TEntity, TEntityHistory>(Expression<Func<TEntity, bool>> condition, int? internalUserID)
            where TEntity : class, IBaseEntity
            where TEntityHistory : class, IBaseEntity, IEntityHistory, new()
        {
            IQueryable<TEntity> entities = Connection.Set<TEntity>();
            if (condition != null)
            {
                entities = entities.Where(condition);
            }
            int recordsNumberToDelete = entities.Count();
            if (recordsNumberToDelete > 0)
            {
                RecordModelChanges<TEntity, TEntityHistory>(entities, internalUserID, true);
                Connection.Set<TEntity>().RemoveRange(entities);
            }
            return recordsNumberToDelete;
        }

        /// <summary>
        /// Delete All Elements where key ID property has a Guid type
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="condition">Delete only entities satisfying the condition</param>|
        /// <param name="internalUserID">User ID in the application database</param>
        /// <returns>Number of deleted entities</returns>
        public int DeleteAllGuid<TEntity, TEntityHistory>(Expression<Func<TEntity, bool>> condition, int? internalUserID)
            where TEntity : class, IBaseGuidEntity
            where TEntityHistory : class, IBaseGuidEntity, IGuidEntityHistory, new()
        {
            IQueryable<TEntity> entities = Connection.Set<TEntity>();
            if (condition != null)
            {
                entities = entities.Where(condition);
            }
            int recordsNumberToDelete = entities.Count();
            if (recordsNumberToDelete > 0)
            {
                RecordModelChangesGuid<TEntity, TEntityHistory>(entities, internalUserID, true);
                Connection.Set<TEntity>().RemoveRange(entities);
            }
            return recordsNumberToDelete;
        }

        /// <summary>
        /// Delete All Elements without history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="condition">Delete only entities satisfying the condition</param>|
        /// <returns>Number of deleted entities</returns>
        public int DeleteAll<TEntity>(Expression<Func<TEntity, bool>> condition) where TEntity : class
        {
            IQueryable<TEntity> entities = Connection.Set<TEntity>();
            if (condition != null)
            {
                entities = entities.Where(condition);
            }
            int recordsNumberToDelete = entities.Count();
            if (recordsNumberToDelete > 0)
            {
                Connection.Set<TEntity>().RemoveRange(entities);
            }
            return recordsNumberToDelete;
        }

        /// <summary>
        /// Saves all changes made in this context to the underlying database with validation
        /// </summary>
        /// <returns></returns>
        public int SaveChanges()
        {
            return Connection.SaveChangesWithErrors();
        }
        /// <summary>
        /// Saves all changes made in this context to the underlying database with validation. Async
        /// </summary>
        /// <returns></returns>
        public async Task<int> SaveChangesAsync()
        {
            return await Connection.SaveChangesWithErrorsAsync();
        }

        /// <summary>
        /// Save original records to history table and update current records with new version number, update date and user ID
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="models">Entities for changes tracking</param>
        /// <param name="internalUserID">User ID in the application database</param>
        /// <param name="wasDeleted">Are entities were deleted</param>
        internal void RecordModelChanges<TEntity, TEntityHistory>(IEnumerable<TEntity> models, int? internalUserID,
            bool wasDeleted = false)
            where TEntity : class, IBaseEntity
            where TEntityHistory : class, IBaseEntity, IEntityHistory, new()
        {
            if (!models.Any())
                throw new ArgumentNullException("Cannot save changes history. Empty record set for " +
                                                typeof(TEntity).Name);

            //If first model in dataset not new (all models should be new or not new) 
            if (models.FirstOrDefault().ID != 0)
            {
                if (!wasDeleted)
                {
                    //Update record version, user and change date 
                    models.ForEach(t =>
                    {
                        t.UpdatedAt = DateTimeOffset.UtcNow;
                        t.RecordVersion = t.RecordVersion + 1;
                        t.UpdatedByID = internalUserID;
                    });
                }
                string tableName = Connection.GetTableName<TEntity>();
                string tableHistoryName = Connection.GetTableName<TEntityHistory>();
                //logger.Info("Table name:" + tableName + ", history: " + tableHistoryName);
                //Run stored procedure
                string sqlCommand =
                    "DECLARE @columns varchar(max) " +
                    "SELECT @columns = coalesce(@columns +	',', '') + convert(varchar(max),COLUMN_NAME) " +
                    "FROM INFORMATION_SCHEMA.COLUMNS " +
                    "WHERE TABLE_NAME = '" + tableHistoryName.Split('.')[1] + "' AND TABLE_SCHEMA='" + tableHistoryName.Split('.')[0] + "' " +
                    "AND COLUMN_NAME not in ('ID','BaseTableID','DeletedByID','DeletedAt') " +
                    "EXEC('SELECT ID,'+@columns+' INTO #tempTable FROM " + tableName + " WHERE ID in (" + string.Join(",", models.Select(t => t.ID)) + ") " +
                    "	IF (SELECT COUNT(*) FROM #tempTable) > 0 " +
                    "	BEGIN " +
                    "       INSERT INTO " + tableHistoryName + "('+@columns+',BaseTableID,DeletedByID,DeletedAt) " +
                    "	    SELECT '+@columns+',ID," + (wasDeleted ? (internalUserID == null ? "NULL" : internalUserID.ToString()) + ",SYSDATETIMEOFFSET()" : "NULL,NULL") + " FROM #tempTable " +
                    "	END " +
                    "')";

                //Logger.Info(sqlCommand);
                int rowsAffected = Connection.ExecuteSql(sqlCommand);
            }
            else //New model
            {
                //Update record version, user and change date 
                DateTimeOffset timeNow = DateTimeOffset.UtcNow;
                models.ForEach(t =>
                {
                    t.UpdatedAt = timeNow;
                    t.CreatedAt = timeNow;
                    t.RecordVersion = 1;
                    t.UpdatedByID = internalUserID;
                });
            }
        }

        /// <summary>
        /// Save original record to history table and update current record with new version number, update date and user ID
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="model">Entity for changes tracking</param>
        /// <param name="internalUserID">User ID in the application database</param>
        /// <param name="wasDeleted">Are entities were deleted</param>
        internal void RecordModelChanges<TEntity, TEntityHistory>(TEntity model, int? internalUserID, bool wasDeleted = false)
            where TEntity : class, IBaseEntity
            where TEntityHistory : class, IBaseEntity, IEntityHistory, new()
        {
            RecordModelChanges<TEntity, TEntityHistory>(new List<TEntity> { model }, internalUserID, wasDeleted);
        }


        /// <summary>
        /// Save original records with Guid ID key propery to history table and update current records with new version number, update date and user ID
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="models">Entities for changes tracking</param>
        /// <param name="internalUserID">User ID in the application database</param>
        /// <param name="wasDeleted">Are entities were deleted</param>
        internal void RecordModelChangesGuid<TEntity, TEntityHistory>(IEnumerable<TEntity> models, int? internalUserID,
            bool wasDeleted = false)
            where TEntity : class, IBaseGuidEntity
            where TEntityHistory : class, IBaseGuidEntity, IGuidEntityHistory, new()
        {
            if (!models.Any())
                throw new ArgumentNullException("Cannot save changes history. Empty record set for " +
                                                typeof(TEntity).Name);

            //If first model in dataset not new (all models should be new or not new) 
            if (models.FirstOrDefault().ID != Guid.Empty)
            {
                if (!wasDeleted)
                {
                    //Update record version, user and change date 
                    models.ForEach(t =>
                    {
                        t.UpdatedAt = DateTimeOffset.UtcNow;
                        t.RecordVersion = t.RecordVersion + 1;
                        t.UpdatedByID = internalUserID;
                    });
                }
                string tableName = Connection.GetTableName<TEntity>();
                string tableHistoryName = Connection.GetTableName<TEntityHistory>();
                //logger.Info("Table name:" + tableName + ", history: " + tableHistoryName);
                List<string> guidList = new List<string>();
                foreach (Guid guid in models.Select(t => t.ID))
                {
                    guidList.Add("''" + guid + "''");
                }
                //Run stored procedure
                string sqlCommand =
                    "DECLARE @columns varchar(max) " +
                    "SELECT @columns = coalesce(@columns +	',', '') + convert(varchar(max),COLUMN_NAME) " +
                    "FROM INFORMATION_SCHEMA.COLUMNS " +
                    "WHERE TABLE_NAME = '" + tableHistoryName.Split('.')[1] + "' AND TABLE_SCHEMA='" + tableHistoryName.Split('.')[0] + "' " +
                    "AND COLUMN_NAME not in ('ID','BaseTableID','DeletedByID','DeletedAt') " +
                    "EXEC('SELECT ID,'+@columns+' INTO #tempTable FROM " + tableName + " WHERE ID in (" + string.Join(",", string.Join(",", guidList)) + ") " +
                    "	IF (SELECT COUNT(*) FROM #tempTable) > 0 " +
                    "	BEGIN " +
                    "       INSERT INTO " + tableHistoryName + "('+@columns+',BaseTableID,DeletedByID,DeletedAt) " +
                    "	    SELECT '+@columns+',ID," + (wasDeleted ? (internalUserID == null ? "NULL" : internalUserID.ToString()) + ",SYSDATETIMEOFFSET()" : "NULL,NULL") + " FROM #tempTable " +
                    "	END " +
                    "')";
                //Logger.Info(sqlCommand);
                int rowsAffected = Connection.ExecuteSql(sqlCommand);
            }
            else //New model
            {
                //Update record version, user and change date 
                DateTimeOffset timeNow = DateTimeOffset.UtcNow;
                models.ForEach(t =>
                {
                    t.UpdatedAt = timeNow;
                    t.CreatedAt = timeNow;
                    t.RecordVersion = 1;
                    t.UpdatedByID = internalUserID;
                });
            }
        }

        /// <summary>
        /// Save original record with Guid ID key propery to history table and update current record with new version number, update date and user ID
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="model">Entity for changes tracking</param>
        /// <param name="internalUserID">User ID in the application database</param>
        /// <param name="wasDeleted">Are entities were deleted</param>
        internal void RecordModelChangesGuid<TEntity, TEntityHistory>(TEntity model, int? internalUserID, bool wasDeleted = false)
            where TEntity : class, IBaseGuidEntity
            where TEntityHistory : class, IBaseGuidEntity, IGuidEntityHistory, new()
        {
            RecordModelChangesGuid<TEntity, TEntityHistory>(new List<TEntity> { model }, internalUserID, wasDeleted);
        }

    }
}

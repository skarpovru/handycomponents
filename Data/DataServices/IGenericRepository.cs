﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using HandyComponents.Data.Models;

namespace HandyComponents.Data.DataServices
{
    public interface IGenericRepository
    {
        DbContext Connection { get; }

        /// <summary>
        /// Detache model from database context (changes in the model will not be saved after that)
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="model"></param>
        void DetacheModel<TEntity>(ref TEntity model) where TEntity : class;

        /// <summary>
        /// Get Queryable entity set
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="condition"></param>
        /// <returns></returns>
        IQueryable<TEntity> GetQueryable<TEntity>(Expression<Func<TEntity, bool>> condition = null)
            where TEntity : class;

        /// <summary>
        /// Get entity by key async
        /// </summary>
        /// <typeparam name="TEntity">Any entity type in the database</typeparam>
        /// <param name="key">ID int</param>
        /// <returns>Entity</returns>
        Task<TEntity> FindAsync<TEntity>(int key)
            where TEntity : class;

        /// <summary>
        /// Update entity with history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="model">Entity of base type where key ID property has an int type</param>
        /// <param name="internalUserID">User ID in the application database</param>
        void Create<TEntity, TEntityHistory>(TEntity model, int? internalUserID)
            where TEntity : class, IBaseEntity
            where TEntityHistory : class, IBaseEntity, IEntityHistory, new();

        /// <summary>
        /// Update entity where key ID property has a Guid type with history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="model">Entity of base type where key ID property has a Guid type</param>
        /// <param name="internalUserID">User ID in the application database</param>
        void CreateGuid<TEntity, TEntityHistory>(TEntity model, int? internalUserID)
            where TEntity : class, IBaseGuidEntity
            where TEntityHistory : class, IBaseGuidEntity, IGuidEntityHistory, new();

        /// <summary>
        /// Update entity without history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="model">Entity of base type where key ID property has an int type</param>
        void Create<TEntity>(TEntity model) where TEntity : class;

        /// <summary>
        /// Update entity with history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="model">Entity of base type where key ID property has an int type</param>
        /// <param name="internalUserID">User ID in the application database</param>
        void Update<TEntity, TEntityHistory>(TEntity model, int? internalUserID)
            where TEntity : class, IBaseEntity
            where TEntityHistory : class, IBaseEntity, IEntityHistory, new();

        /// <summary>
        /// Update entities with history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="models">Entities of base type where key ID property has an int type</param>
        /// <param name="internalUserID">User ID in the application database</param>
        void Update<TEntity, TEntityHistory>(IEnumerable<TEntity> models, int? internalUserID)
            where TEntity : class, IBaseEntity
            where TEntityHistory : class, IBaseEntity, IEntityHistory, new();

        /// <summary>
        /// Update entity where key ID property has a Guid type with history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="model">Entity of base type where key ID property has a Guid type</param>
        /// <param name="internalUserID">User ID in the application database</param>
        void UpdateGuid<TEntity, TEntityHistory>(TEntity model, int? internalUserID)
            where TEntity : class, IBaseGuidEntity
            where TEntityHistory : class, IBaseGuidEntity, IGuidEntityHistory, new();

        /// <summary>
        /// Update entities where key ID property has a Guid type with history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="models">Entities of base type where key ID property has a Guid type</param>
        /// <param name="internalUserID">User ID in the application database</param>
        void UpdateGuid<TEntity, TEntityHistory>(IEnumerable<TEntity> models, int? internalUserID)
            where TEntity : class, IBaseGuidEntity
            where TEntityHistory : class, IBaseGuidEntity, IGuidEntityHistory, new();

        /// <summary>
        /// Delete entity with history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="model">Entity of base type where key ID property has an int type</param>
        /// <param name="internalUserID">User ID in the application database</param>
        void Delete<TEntity, TEntityHistory>(TEntity model, int? internalUserID)
            where TEntity : class, IBaseEntity
            where TEntityHistory : class, IBaseEntity, IEntityHistory, new();

        /// <summary>
        /// Delete entity where key ID property has a Guid type with history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="model">Entity of base type where key ID property has a Guid type</param>
        /// <param name="internalUserID">User ID in the application database</param>
        void DeleteGuid<TEntity, TEntityHistory>(TEntity model, int? internalUserID)
            where TEntity : class, IBaseGuidEntity
            where TEntityHistory : class, IBaseGuidEntity, IGuidEntityHistory, new();

        /// <summary>
        /// Delete entity without history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="model">Entity of base type where key ID property has an int type</param>
        void Delete<TEntity>(TEntity model) where TEntity : class;

        /// <summary>
        /// Delete All Elements
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="condition">Delete only entities satisfying the condition</param>|
        /// <param name="internalUserID">User ID in the application database</param>
        /// <returns>Number of deleted entities</returns>
        int DeleteAll<TEntity, TEntityHistory>(Expression<Func<TEntity, bool>> condition, int? internalUserID)
            where TEntity : class, IBaseEntity
            where TEntityHistory : class, IBaseEntity, IEntityHistory, new();

        /// <summary>
        /// Delete All Elements where key ID property has a Guid type
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TEntityHistory">Entity audit table type</typeparam>
        /// <param name="condition">Delete only entities satisfying the condition</param>|
        /// <param name="internalUserID">User ID in the application database</param>
        /// <returns>Number of deleted entities</returns>
        int DeleteAllGuid<TEntity, TEntityHistory>(Expression<Func<TEntity, bool>> condition, int? internalUserID)
            where TEntity : class, IBaseGuidEntity
            where TEntityHistory : class, IBaseGuidEntity, IGuidEntityHistory, new();

        /// <summary>
        /// Delete All Elements without history tracking
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="condition">Delete only entities satisfying the condition</param>|
        /// <returns>Number of deleted entities</returns>
        int DeleteAll<TEntity>(Expression<Func<TEntity, bool>> condition) where TEntity : class;

        /// <summary>
        /// Saves all changes made in this context to the underlying database with validation
        /// </summary>
        /// <returns></returns>
        int SaveChanges();

        /// <summary>
        /// Saves all changes made in this context to the underlying database with validation. Async
        /// </summary>
        /// <returns></returns>
        Task<int> SaveChangesAsync();
    }
}
